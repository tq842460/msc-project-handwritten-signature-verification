import numpy as np
import pandas as pd
from skimage.morphology import skeletonize
import cv2

############################
# read the image from the given path as grayscale image
def read_img (path):
    im = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
    return im

############################
# clean the image of fuzzy noise so image is smoother
#   with the aim that the background is one homogenous shade
def deionise_img (im):
    nl_im = cv2.fastNlMeansDenoising(im, None, 20, 5, 5)
    return nl_im

############################
# convert image to black and white
# not done initially to ensure clean image and all signature features are captured
def to_bw_img (im):
    bw_im = cv2.threshold(im, 128, 255, cv2.THRESH_OTSU)
    # threshold function returns an array containing the threshold of split between
    #   black and white as well as the converted image
    return bw_im[1]

############################
# invert image between black and white, ready for wireframing
def invert_img (im):
    inv_im = cv2.bitwise_not(im)/255
    return inv_im

############################
# generate wireframe of signature to enhance features of signature
def skeletonise_img(im):
    sk_im = skeletonize(im)
    return sk_im

############################
# used when correcting baseline slant
# the left most pixel coordinate is found, bottom most if tied equal left
# coordinates are in [y,x] order origin [0,0] at top left
def left_most_pix (im):
    for i in range(im.shape[1]):
        for j in range(im.shape[0]):
            k = im.shape[0] - j - 1
            if im[k][i] == 1:
                return [k,i]

# used when correcting baseline slant
# the right most pixel coordinate is found, top most if tied equal right
# coordinates are in [y,x] order origin [0,0] at top left
def right_most_pix (im):
    for i in range(im.shape[1]):
        k = im.shape[1] - i - 1
        for j in range(im.shape[0]):
            if im[j][k] == 1:
                return [j,k]

# used when correcting baseline slant
# find the angle of change needed in baseline slant correction based on the
#   extreme left and right pixels
# tan of dy/dx in a right angle triangle
# (order of left or right does not matter so long as each the side sequence is the same)
# (-1/-1 = 1/1)
# angle is negative for clockwise rotation
# angle returned as both degrees and radians
def find_slant_angle (im):
    lmp = left_most_pix(im)
    rmp = right_most_pix(im)
    angle_r = np.arctan((lmp[0]-rmp[0])/(lmp[1]-rmp[1]))
    angle_d = angle_r * 180 / np.pi
    return {"deg": angle_d, "rad": angle_r}

############################
# used when correcting baseline slant
# during rotation, pixels may be dropped as the resulting image keeps the same size
# padding the image will keep the dropped pixels
# calculate padding required by using cosine and sine rule with radian angle and image shape
#   in a right angle triangle

# used when correcting baseline slant
# topbot padding can be calculated using a function of the original width and height
# angle must be in radians
# returns value rounded to nearest integer as an int
def topbot_pad (im, angle_r):
    o_height, o_width = im.shape
    angle_r_90 = abs(angle_r)%((np.pi)/2)
    n_height = o_width*np.cos(angle_r_90) + o_height*np.sin(angle_r_90)
    tb_pad = (n_height - o_height)/2
    #tb_pad = (im.shape[1]//2) * np.sin(abs(angle_r)) / np.sin((np.pi)/2)
    tb_pad = int(np.ceil(tb_pad))
    return tb_pad

# used when correcting baseline slant
# rl padding can be calculated using a function of the original width and height
# angle must be in radians
# returns value rounded to nearest integer
def rightleft_pad (im, angle_r):
    o_height, o_width = im.shape
    angle_r_90 = abs(angle_r)%((np.pi)/2)
    n_width = o_width*np.sin(angle_r_90) + o_height*np.cos(angle_r_90)
    rl_pad = (n_width - o_width)/2
    #rl_pad = (im.shape[0]//2) * np.sin(abs(angle_r)) / np.sin((np.pi)/2)
    rl_pad = int(np.ceil(rl_pad))
    return rl_pad

# used when correcting baseline slant
# adds padding to the image based on the calculated padding required by rotation
# maximum padding is used to ensure all signature pixels are retained
# extra five added in case the signature is touching the edge which if not added will 
#   result in lack of border when cropping later
def add_pad (im, angle_r):
    pad_im = np.pad(im, max(topbot_pad(im, angle_r), rightleft_pad(im, angle_r)),
                    'constant')
    return pad_im

############################
# correct the baseline slant of the signature through rotation
# first find the angle, the pad the image in preparation to keep all pixels
# image needs to be in uint8 format for warpAffine to work
# calculate the affine transform mask for rotation
# rotate the image using the mask whilst keeping the same scale
# warpAffine works in degrees, whilst padding works in radians
# two arguements to allow option to rotate unskeletonised image
def correct_baseline_slant_img (im, skel_im):
    angle_dict = find_slant_angle(skel_im)
    pad_im = add_pad(im, angle_dict["rad"])
    conv_pad_im = np.array(pad_im, dtype="uint8")
    r_mask = cv2.getRotationMatrix2D((conv_pad_im.shape[1]//2, conv_pad_im.shape[0]//2),
                                     angle_dict["deg"], 1)
    r_im = cv2.warpAffine(conv_pad_im, r_mask,
                          (conv_pad_im.shape[1], conv_pad_im.shape[0]))
    return r_im

############################
# next four functions find the indices for row or column where signature pixels
#   begin, keeping a one pixel margin
# top and both loops from top and bottom respectively
# left and right transposes the image and then loops from top and bottom repectively
def top_brk_pnt (im):
    for i in range(im.shape[0]):
        if im[i].any() != 0:
            return i-1
        
def bot_brk_pnt (im):
    for i in range(im.shape[0]):
        j = im.shape[0] - i - 1
        if im[j].any() != 0:
            return j+2

def l_brk_pnt (im):
    im_t = np.transpose(im)
    for i in range(im_t.shape[0]):
        if im_t[i].any() != 0:
            return i-1

def r_brk_pnt (im):
    im_t = np.transpose(im)
    for i in range(im_t.shape[0]):
        j = im_t.shape[0] - i - 1
        if im_t[j].any() != 0:
            return j+2

# using the above four functions crop the signature with a one pixel margin
# slicing at the indicies are used for top and bottom, but this is not possible
#   for left and right, therefore np.delete is used with slicing
def crop_img (im):
    crp_im = im[top_brk_pnt(im):bot_brk_pnt(im), l_brk_pnt(im):r_brk_pnt(im)]
    return crp_im

############################
# resize the image to a target size
def resize_img (im, target):
    rs_im = cv2.resize(im, target)
    return rs_im

############################
# used to generate a waveform of an image
# generate waveform section given the image and a row number (y)
# now uses the column number of each white pixel as its value
# return the row
def gen_waveform_row (im, y):
    # initialise the grey and black containers
    im_gry = []
    im_blk = []
    
    # loop for each column pixel in the row (y)
    for i in range(im.shape[1]):
        # test pixel value and append to containers accordingly
        if im[y][i] == 1:
            im_gry.append(i)
        else:
            im_blk.append(0)
    
    # set containers to numpy arrays of uint8 format
    im_gry = np.array(im_gry, dtype="uint8")
    im_blk = np.array(im_blk, dtype="uint8")
    
    # return as a concatenated array with grey pixels first
    # array needs to be in one dimension so is returned in one array[]
    return [np.concatenate((im_gry, im_blk), axis=None)]

# generate a waveform of the image by counting the number of white pixels in each row
# the grey pixels are essentially moved to the left of the image to form a waveform
def generate_waveform (im):
    # the first row is transformed and used for initialisation of the waveform
    wf_im = np.array(np.array(gen_waveform_row(im, 0), dtype="uint8"))
    
    # subsequent waveform rows can be concatenated in a loop
    # loop for each row in the image starting at the second row
    for i in range(1, im.shape[0]):
        wf_im = np.concatenate((wf_im, np.array(gen_waveform_row(im, i))), axis=0)
    
    # image is transposed to create a vertical waveform
    wf_im = np.transpose(wf_im)
    return wf_im

############################
# used to track the waveform in the image
# find the index for the top most black pixel in each column (x)
def find_top_blk (im, x):
    for i in range(im.shape[0]):
        if im[i][x] == 0:
            return i

# tracks the waveform of the image
# stores the index of the top most pixel in each column as a 1D-array
def track_waveform (im):
    waveform = np.array([], dtype='uint8')
    for i in range(im.shape[1]):
        waveform = np.append(waveform, find_top_blk(im, i))
    return waveform

############################
# instead of tracking the waveform, it can be flattened by row (row1 then row2 and so on)
# KLD can then be done on the full length
def flatten_waveform (im):
    im_f = im.flatten()
    return im_f

############################
# generate a 1-D array representing the waveform of the image at the given path
# uses skeletonised image and tracks the waveform
def wf_blackbox_track_skel (path, targ_size):
    im = read_img(path)
    im = deionise_img(im)
    im = to_bw_img(im)
    im = invert_img(im)
    skel_im = skeletonise_img(im)
    im = correct_baseline_slant_img(skel_im, skel_im)
    im = crop_img(im)
    target = (targ_size, targ_size)
    im = resize_img(im, target)
    im = generate_waveform(im)
    wf = track_waveform(im)
    return wf

# generate a 1-D array representing the waveform of the image at the given path
# uses un-skeletonised image and tracks the waveform
def wf_blackbox_track_unskel (path, targ_size):
    im = read_img(path)
    im = deionise_img(im)
    im = to_bw_img(im)
    im = invert_img(im)
    skel_im = skeletonise_img(im)
    im = correct_baseline_slant_img(im, skel_im)
    im = crop_img(im)
    target = (targ_size, targ_size)
    im = resize_img(im, target)
    im = generate_waveform(im)
    wf = track_waveform(im)
    return wf

# generate a 1-D array representing the waveform of the image at the given path
# uses skeletonised image and flattens the waveform
def wf_blackbox_flat_skel (path, targ_size):
    im = read_img(path)
    im = deionise_img(im)
    im = to_bw_img(im)
    im = invert_img(im)
    skel_im = skeletonise_img(im)
    im = correct_baseline_slant_img(skel_im, skel_im)
    im = crop_img(im)
    target = (targ_size, targ_size)
    im = resize_img(im, target)
    im = generate_waveform(im)
    wf = flatten_waveform(im)
    return wf

# generate a 1-D array representing the waveform of the image at the given path
# uses un-skeletonised image and flattens the waveform
def wf_blackbox_flat_unskel (path, targ_size):
    im = read_img(path)
    im = deionise_img(im)
    im = to_bw_img(im)
    im = invert_img(im)
    skel_im = skeletonise_img(im)
    im = correct_baseline_slant_img(im, skel_im)
    im = crop_img(im)
    target = (targ_size, targ_size)
    im = resize_img(im, target)
    im = generate_waveform(im)
    wf = flatten_waveform(im)
    return wf

############################
# calculate average wf for each person given a dataframe of waveforms that have been indexed
# indexing has been hardcoded for now
# initialises average wf dataframe using the first person
def wf_ave (wfs):
    ave_wfs = np.reshape(wfs[wfs["Person"] == "P01"].iloc[:,2:].mean(axis=0).to_numpy(),(1,len(wfs.columns)-2))
    for person in range(1,55):
        ave_wfs = np.append(ave_wfs, 
                        np.reshape(wfs[wfs["Person"] == f"P{person+1:02d}"].iloc[:,2:].mean(axis=0).to_numpy(),
                                   (1,len(wfs.columns)-2)), axis=0)
    ave_data = pd.DataFrame(data=ave_wfs)
    return ave_data