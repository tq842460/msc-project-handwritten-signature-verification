Hi,

This repository is for CSMPR21 23-34A St. ID:31842460

It is laid out with all files that would have been created from the python notebooks

First is the KLD VFin.ipynb file. it covers all the entire method, from removing images
marked for removal (using remove-ref.txt), to generating the waveforms, to normalising
the dissimilarities.

Second is the Learning.ipynb file which covers the inspection of normalised dissimilarity
spread and evaluation in writer dependent and independent settings.

Third is the Classification and Code Demo.ipynb which covers the demonstration of ability
to verify signatures.

Please feel free to try different variants and signature settings.

Kind Regards,
Nelson Wong