import numpy as np
import pandas as pd

# Adds a negligible amount to all values to prevent divide by zero error
def non_zero (wf):
    wf = wf+0.000001 # adjust as necessary
    return wf

# Calculates the probability distribution of the waveform
def prob_wf (wf):
    s_wf = sum(wf)
    p_wf = wf/s_wf
    return p_wf

# Calculates PSI given two waveforms assuming waveforms include zero values
def PSI (wf, ref_wf):
    wf, ref_wf = non_zero(wf),non_zero(ref_wf)
    p_wf, p_ref_wf = prob_wf(wf),prob_wf(ref_wf)
    d_wfs = []
    for i in range(p_wf.shape[0]):
        d_wfs.append((p_wf[i]-p_ref_wf[i]) * np.log2(p_wf[i]/p_ref_wf[i]))
    return sum(d_wfs), d_wfs

# Calculates KLD given two waveforms assuming waveforms include zero values
def KLD (wf, ref_wf):
    wf, ref_wf = non_zero(wf),non_zero(ref_wf)
    p_wf, p_ref_wf = prob_wf(wf),prob_wf(ref_wf)
    d_wfs = []
    for i in range(p_wf.shape[0]):
        d_wfs.append(p_wf[i] * np.log2(p_wf[i]/p_ref_wf[i]))
    return sum(d_wfs), d_wfs

# Used to obtain the minimum and maximum values in the given dataframe
def min_max_val (df):
    return df.iloc[:,3:].min().min(), df.iloc[:,3:].max().max()

# Used to normalise all values in the given dataframe between 0 and 1
# Also returns the min and max values in that dataframe
# Min and Max will be saved in a separate library
def norm_df (div_df):
    df_min, df_max = min_max_val (div_df)
    div_norm_data = (div_df.iloc[:,3:] - df_min) / (df_max - df_min)
    div_norm = pd.concat([div_df.iloc[:,:3], div_norm_data], axis=1)
    return div_norm, df_min, df_max

# Used to normalise a divergence measure given a min and max
# Only used for singular values
# Min and Max will be extracted from the min max library
def norm_div (div, div_min, div_max):
    norm = (div - div_min) / (div_max - div_min)
    return norm